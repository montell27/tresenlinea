<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//versión 1 del api de partida y jugadas
Route::prefix('v1')->group(function () {
//Route::resource('partida','PartidaController');
//Route::resource('jugadas','JugadasController');

Route::post('nuevaPartida', 'PartidaController@create');
Route::post('nuevaJugada', 'JugadasController@create');
Route::post('unirsePartida', 'PartidaController@unirse');
Route::post('cambiarNombre', 'PartidaController@cambioNombreJugador');

});