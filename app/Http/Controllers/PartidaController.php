<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partida;
class PartidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$nuevaPartida=Partida::create();
		$Partida=Partida::find($nuevaPartida->id);
		return response()->json(['status'=>'ok','data'=>$Partida], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		return view('welcome');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
	public function unirse(Request $request)
    {
        $validator = \Validator::make($request->all(),  [
			'idPartida' => 'required|numeric',
			
			'jugador' => 'required|numeric|min:2|max:2'
			],       $messages = [
			'idPartida.required' => 'idPartida es requerido',
			
			'jugador.required' => 'jugador es requerido',
			'idPartida.numeric' => 'idPartida debe ser un numero',
			
			'jugador.numeric' => 'jugador debe ser un numero'
			]);
			
			if ($validator->fails()) { 
				return response()->json(['errors'=>$validator->errors()->all()],404);
			}
			
			//validar que el idPartida existe y no esta finalizada
			$Partida=Partida::find($idPartida);

			if (! $Partida)
			{
				return response()->json(['errors'=>['idPartida no encontrado']],404);
			}else if($Partida->fin==1){
				return response()->json(['errors'=>['Partida Finalizada no puede unirse a jugada']],404);
			}
			
			$cantidadJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $Partida->games
			])->count();
			if($cantidadJugadas>=9){
				return response()->json(['errors'=>['Partida Finalizada no puede unirse a jugada']],404);
			}
			$detalleJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $request->games
			])->get();
			return response()->json(['status'=>'ok','partida'=>$Partida,'jugadas'=>$detalleJugadas], 200);
    }
	public function cambioNombreJugador(Request $request)
    {
        $validator = \Validator::make($request->all(),  [
			'idPartida' => 'required|numeric',
			
			'jugador' => 'required|numeric|min:1|max:2',
			'nombre' => 'required'
			],       $messages = [
			'idPartida.required' => 'idPartida es requerido',
			'nombre.required' => 'Para cambiar el nombre el texto es requerido',
			'jugador.required' => 'jugador es requerido',
			'idPartida.numeric' => 'idPartida debe ser un numero',
			
			'jugador.numeric' => 'jugador debe ser un numero'
			]);
			
			if ($validator->fails()) { 
				return response()->json(['errors'=>$validator->errors()->all()],404);
			}
			
			//validar que el idPartida existe y no esta finalizada
			$Partida=Partida::find($idPartida);

			if (! $Partida)
			{
				return response()->json(['errors'=>['idPartida no encontrado']],404);
			}else if($Partida->fin==1){
				return response()->json(['errors'=>['Partida Finalizada no puede registrar Nombre']],404);
			}
			
			$cantidadJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $Partida->games
			])->count();
			if($cantidadJugadas>=9){
				return response()->json(['errors'=>['Partida Finalizada no puede registrar Nombre']],404);
			}
			if($request->jugador==1){
				$Partida->creador=$request->nombre;
			}else{
				$Partida->invitado=$request->nombre;
			}
			
			$Partida->save();
			
			$detalleJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $request->games
			])->get();
			return response()->json(['status'=>'ok','partida'=>$Partida,'jugadas'=>$detalleJugadas], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
