<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partida;
use App\Models\Jugadas;

class JugadasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		return "";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = \Validator::make($request->all(),  [
			'idPartida' => 'required|numeric',
			'games' => 'required|numeric',
			'jugador' => 'required|numeric|min:1|max:2',
			'posicion' => 'required'
			],       $messages = [
			'idPartida.required' => 'idPartida es requerido',
			'games.required' => 'games es requerido',
			'jugador.required' => 'jugador es requerido',
			'posicion.required' => 'posicion es requerido',
			'idPartida.numeric' => 'idPartida debe ser un numero',
			'games.numeric' => 'games debe ser un numero',
			'jugador.numeric' => 'jugador debe ser un numero 1 creador 2 invitado'
			]);
			
			if ($validator->fails()) { 
				return response()->json(['errors'=>$validator->errors()->all()],404);
			}
			
			//validar que el idPartida existe y no esta finalizada
			$Partida=Partida::find($idPartida);

			if (! $Partida)
			{
				return response()->json(['errors'=>['idPartida no encontrado']],404);
			}else if($Partida->fin==1){
				return response()->json(['errors'=>['Partida Finalizada no puede registrar jugada']],404);
			}
			
			//validar que la partida y games son menores a 9 y que el jugador le toca jugar
			$cantidadJugadas=0;
			$cantidadJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $request->games
			])->count();
			if($cantidadJugadas>=9){
				return response()->json(['errors'=>['Partida Finalizada no puede registrar jugada']],404);
			}
			$ultimoJugadorJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $request->games
			])->orderBy('id', 'desc')->limit(1)->get();
			
			if($ultimoJugadorJugadas->jugador==$request->jugador){
				return response()->json(['errors'=>['Jugador no corresponde la jugada, no puede registrar jugada']],404);
			}
			
			//registrar la jugada
			$nuevoJugada=Jugadas::create($request->all());
			
			//validar si hay ganador -> si hay nueve jugadas sin ganador hay empate , si hay ganador o empate finalizar la partida 
			$detalleJugadas = Jugadas::where([
				   'idPartida' => $request->idPartida,
				   'games' => $request->games
			])->get();
			//inicializo vectores ganadores
			for ($ii = 1; $ii <= 2; ++$ii ){
				for($i = 1; $i <= 3; ++$i) {
					$vectorH[$ii][$i]=0;
					$vectorV[$ii][$i]=0;
					$vectorD[$ii][$i]=0;
				}
			}
			$contador=0;
			foreach($detalleJugadas as $item){
				$contador++;
				$p=explode(",",$item->posicion);
				$vectorH[$item->jugador][$p[0]]+=1;
				$vectorV[$item->jugador][$p[1]]+=1;
				
				if(($p[0]==$p[1]) or ($p[0]==1 and $p[1]==3) or ($p[0]==3 and $p[1]==1)){
					$vectorD[$item->jugador]+=1;
				}
				// valida ganador
				if($vectorH[$item->jugador][$p[0]]==3 or $vectorV[$item->jugador][$p[1]]==3 or $vectorD[$item->jugador]==3){
					//hay ganador actualizar partida y retornar detalle jugadas
					$Partida->fin=1;
					if($request->jugador==1){
						$Partida->ganadasCreador+=1;
					}else{
						$Partida->ganadasInvitado+=1;
					}
					$Partida->save();
					return response()->json(['status'=>'ok','partida'=>$Partida,'jugadas'=>$detalleJugadas], 200);
					
				}
				
			}
			if($contador==9){
				//no hay ganador actualizar partida y retornar detalle jugadas
				$Partida->fin=1;
				$Partida->empates+=1;
				$Partida->save();
				return response()->json(['status'=>'ok','partida'=>$Partida,'jugadas'=>$detalleJugadas], 200);
			}
			// aun no termina pa partida se retornan las jugadas
		return response()->json(['status'=>'ok','partida'=>$Partida,'jugadas'=>$detalleJugadas], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		return "";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		return "";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
