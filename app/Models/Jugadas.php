<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jugadas extends Model
{
     protected $table="jugadas"; // nombre de la tabla donde se llevaran las jugadas de las partida
	protected $fillable = array('idPartida','games','jugador','posicion','jugadaNumero'); 
	// idPartida id relacionada con la partida
	// games indica el juego que corresponde esta jugada
	// jugador 1 creador y 2 invitado define el dueño de la jugada
	// posicion json define coordenadas de la jugada
	// jugadaNumero lleva las jugadas de este juego y partida minimo 1 maximo 9
	
	protected $hidden = ['created_at'];
	public function partida()
	{	
		// 1 jugada pertenece a una partida
		// $this hace referencia al objeto que tengamos en ese momento de Jugadas.
		return $this->belongsTo('App\Models\Partida');
	}
}
