<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    protected $table="partida"; // nombre de la tabla donde se llevaran las partidas
	protected $fillable = array('creador','invitado','games','iniciaPartida','ganadasCreador','ganadasInvitado','empates','fin'); 
	// Creador Jugador que inicia la Partida inicia en Jugador 1
	// Invitado jugador oponente del Creador inicia en Jugador 2
	// games contador de juegos ralizados en cada partida inicia en 1
	// IniciaPartida quien le toca iniciar la partida 1 creador y 2 invitado
	// ganadasCreador cantidad de partidas ganadas por creador
	// ganadasInvitado cantidad de partidas ganadas por invitado
	// empates cantidad de pjuegos empatados en la partida
	// fin valor binario para definir fin del juego fin=1 si no 0
	
	protected $hidden = ['created_at'];
	public function jugadas()
	{	
		// 1 partida tiene muchas jugadas
		// $this hace referencia al objeto que tengamos en ese momento de Partida.
		return $this->hasMany('App\Models\Jugadas');
	}
}
