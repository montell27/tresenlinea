<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PartidaMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partida', function (Blueprint $table) {
            $table->id();
			$table->string('creador')->default('Jugador 1');
            $table->string('invitado')->default('Jugador 2');
			$table->integer('games')->default(1);
			$table->integer('iniciaPartida')->default(1);
			$table->integer('ganadasCreador')->default(0);
			$table->integer('ganadasInvitado')->default(0);
			$table->integer('empates')->default(0);
			$table->boolean('fin')->default(0);
            $table->timestamps();
			// Creador Jugador que inicia la Partida inicia en Jugador 1
			// Invitado jugador oponente del Creador inicia en Jugador 2
			// games contador de juegos ralizados en cada partida inicia en 1
			// IniciaPartida quien le toca iniciar la partida 1 creador y 2 invitado
			// ganadasCreador cantidad de partidas ganadas por creador
			// ganadasInvitado cantidad de partidas ganadas por invitado
			// empates cantidad de pjuegos empatados en la partida
			// fin valor binario para definir fin del juego fin=1 si no 0
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partida');
    }
}
