<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JugadasMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jugadas', function (Blueprint $table) {
            $table->id();
			$table->integer('idPartida');
            // Indicamos cual es la clave foránea de esta tabla:
            $table->foreign('idPartida')->references('id')->on('partida');
			$table->integer('games');
			$table->integer('jugador');
			$table->string('posicion');
			$table->integer('jugadaNumero');
            $table->timestamps();
			// idPartida id relacionada con la partida
			// games indica el juego que corresponde esta jugada
			// jugador 1 creador y 2 invitado define el dueño de la jugada
			// posicion json define coordenadas de la jugada
			// jugadaNumero lleva las jugadas de este juego y partida minimo 1 maximo 9
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
	 public function down()
    {
        Schema::dropIfExists('jugadas');
    }
}
